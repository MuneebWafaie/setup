
import type { Config } from "drizzle-kit";

export default {
	schema: "./src/db/schema.ts",
	driver: "pg",
	dbCredentials: {
		connectionString: "",
	},
	out: "./drizzle",
} satisfies Config;
