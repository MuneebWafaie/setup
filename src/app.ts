import express from "express"

const PORT = process.env.PORT || 3001;
const app = express();

app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));

const todos: string[] = [];

app.post("/todo", (req, res) => {
  console.log("recieved", req.body);
  todos.push(req.body.todo);
  res.send(insertTodos(todos));
})


app.listen(PORT, () => console.log(`Server is up and running on Port:${PORT}`)) //eslint-disable-line


function insertTodos(todos: string[]) {
  return `<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/output.css">
  <title>Document</title>
</head>

<body>
  <div><ul>${todos.reduce((acc, todo) => acc + `<li>${todo}</li>`, "")}</ul></div>
  <form method="post" action="/todo" enctype="application/x-www-form-urlencoded">
    <input name="todo" type="text" />
    <button type="submit">Save todo</button>
  </form>
</body>

</html>`}
